<?php

use Refluence\ModuloApi\ApiDataValidation;
use Refluence\ModuloApi\DB;

class DBTest extends PHPUnit_Framework_TestCase
{
  protected $columns = ['user_name', 'user_email', 'user_phone', 'user_password', 'user_company'];
  protected $quoted_values = ["'test_name'::varchar", "'email@gmail.com'::varchar", "'093464684'::varchar", "'password'::varchar", "'my_company'::varchar"];

  public function testConnect()
  {
    $this->assertNull(DB::$conn);
    $this->assertEmpty(DB::$conn_string);
    DB::connect();
    $this->assertNull(DB::$conn);

    DB::$conn_string = 'host=localhost port=5432 dbname=triangle_config_dev user=developer password=developer';
    $this->assertNotNull(DB::$conn_string);
    DB::connect();
    $this->assertNotNull(DB::$conn);
    $this->assertInternalType('resource', DB::$conn);
    $this->assertEquals(PGSQL_CONNECTION_OK, pg_connection_status(DB::$conn));
  }

  public function testGetSqlInsertClause()
  {
    $result = DB::getSqlInsertClause($this->columns, $this->quoted_values);
    $this->assertNotEmpty($result);
    $this->assertEquals(
      " (user_name, user_email, user_phone, user_password, user_company) VALUES ('test_name'::varchar, 'email@gmail.com'::varchar, '093464684'::varchar, 'password'::varchar, 'my_company'::varchar) ",
      $result
    );
  }

  public function testGetSqlUpdateClause()
  {
    $result = DB::getSqlUpdateClause($this->columns, $this->quoted_values);
    $this->assertNotEmpty($result);
    $this->assertEquals(
      " user_name = 'test_name'::varchar ,  user_email = 'email@gmail.com'::varchar ,  user_phone = '093464684'::varchar ,  user_password = 'password'::varchar ,  user_company = 'my_company'::varchar ",
      $result
    );
  }

  /**
   * @dataProvider providerSmartQuoteSql
   */
  public function testSmartQuoteSqlValue($type, $value, $result)
  {
    $this->assertEquals($result, DB::smartQuoteSqlValue($type, $value));
  }

  public function providerSmartQuoteSql()
  {
    return [
      [DB::SQL_TEXT,      'test', "'test'::" . DB::SQL_TEXT],
      [DB::SQL_NUM,       'test', "'test'::" . DB::SQL_NUM],
      [DB::SQL_NUMERIC,   'test', "'test'::" . DB::SQL_NUMERIC],
      [DB::SQL_JSON,      'test', "'test'::" . DB::SQL_JSON],
      [DB::SQL_UUID,      'test', "'test'::" . DB::SQL_UUID],
      [DB::SQL_TIME,      'test', "'test'::" . DB::SQL_TIME],
      [DB::SQL_DATE,      'test', "'test'::" . DB::SQL_DATE],
      [DB::SQL_INT,       'test', "'test'::" . DB::SQL_INT],
      [DB::SQL_SINT,      'test', "'test'::" . DB::SQL_SINT],
      [DB::SQL_INTEGER,   'test', "'test'::" . DB::SQL_INTEGER],
      [DB::SQL_INT_ARRAY, 'test', "'test'::" . DB::SQL_INT_ARRAY],
      [DB::SQL_VARCHAR,   'test', "'test'::" . DB::SQL_VARCHAR],
      [DB::SQL_NONE,      'test', "'test'"],
      ['undefined_type',  'test', "'test'"],
      ['',                'test', "'test'"],
      [null,              'test', "'test'"],
      [null,              '', "''"],
    ];
  }
}
