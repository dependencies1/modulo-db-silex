<?php

namespace Refluence\ModuloApi;

class DB
{
    const SQL_TEXT      = 'text';
    const SQL_NUM       = 'numeric';
    const SQL_NUMERIC   = 'numeric';
    const SQL_JSON      = 'json';
    const SQL_UUID      = 'uuid';
    const SQL_TIME      = 'time';
    const SQL_DATE      = 'date';
    const SQL_INT       = 'integer';
    const SQL_SINT      = 'smallint';
    const SQL_INTEGER   = 'integer';
    const SQL_INT_ARRAY = 'integer[]';
    const SQL_VARCHAR   = 'varchar';
    const SQL_VARCHAR_ARRAY = 'varchar[]';
    const SQL_NONE      = 'none'; // =>  not an SQL type. Requires NO validation and NO quoting.

    // PostgreSQL Error Codes
    const SQL_SUCCESSFUL_COMPLETION = '00000';
    const SQL_UNIQUE_VIOLATION      = '23505';

    // Core object
    public static $conn_string    = '';
    public static $conn           = null;
    private static $query_result  = null;

    // Conflict resolution (useful in multi-thread environment with strong locking)
    public static $min_delay = 0.01; // in second
    public static $max_delay = 5.00;

    // Debug options and variables
    public static $last_query     = '';
    public static $debug_options = [
        'debug' => false,
        'debug_dir' => './',
        'log_name' => 'database.log',
        'date_time' => ''
    ];


    /**
     * Log notices or infos
     * @return null|resource
     */
    public static function logInfo($message)
    {
        if (self::$debug_options['debug'])
        {
            file_put_contents(
                self::$debug_options['debug_dir'].self::$debug_options['log_name'],
                'INFO: [' . date('c') . '] ' . $query . "\n",
                FILE_APPEND);        
        }
    }

    /**
     * Log fatal DB errors
     * @return null|resource
     */
    public static function logError($message)
    {
        if (self::$debug_options['debug'])
        {
            file_put_contents(
                self::$debug_options['debug_dir'].self::$debug_options['log_name'],
                'ERROR: [' . date('c') . '] ' . $query . "\n",
                FILE_APPEND);        
        }
    }


    /**
     * @return null|resource
     */
    public static function connect()
    {
        try {
            if (!self::$conn || pg_connection_status(self::$conn) != PGSQL_CONNECTION_OK)
            {
                if (!self::$conn_string) throw new \Exception("conn_string is undefined");
                self::$conn = pg_connect(self::$conn_string);
            }
        } catch (\Exception $e) {
            return false;
            //die("Database connect error, please retry in a few seconds...\n");
        }

        return self::$conn;
    }

    /**
     * @param string $table_name
     * @param string $column_names
     * @param array $array
     * @param array $options
     * @returns boolean
     */
    public static function copyFrom ($table_name, $column_names, $array, $options)
    {
        $query = "COPY " . $table_name;
        if (count($column_names)) {
            $query .= "(" . implode(',', $column_names) . ")";
        }

        $query .= " FROM STDIN ";
        if (count($options))
        {

            $options_arr = [];
            foreach ($options as $option) {
                $options_arr []= $option->name . " " . $option->value;
            }

            $query .= " WITH (" . implode(',', $options_arr) . ")";
        }

        $query .= ";";

        self::logInfo( $query );

        $finishedWithoutError = true;
        try {
            self::connect();
            pg_query(self::$conn, $query);

            foreach ($array as $row) {
                pg_put_line(self::$conn, $row . "\n");
            }

            pg_put_line(self::$conn, "\\.\n");
            $finishedWithoutError = pg_end_copy(self::$conn);
        } catch (\Exception $e) {
            $finishedWithoutError = false;
        }

        return $finishedWithoutError;
    }
    
    /**
     * Returns last error message
     * @return string
     */
    public static function getLastError () {
        return pg_last_error(self::$conn);
    }

    /**
     * @param $columns
     * @param $values
     * @return bool|string
     */
    public static function getSqlInsertClause($columns, $values)
    {
        if (count($columns) !== count($values)) return false;
        return " (" . implode(', ', $columns) . ") VALUES (" . implode(', ', $values) . ") ";
    }

    /**
     * @param $columns
     * @param $values
     * @return bool|string
     */
    public static function getSqlUpdateClause($columns, $values)
    {
        if (count($columns) !== count($values)) return false;
        $str = [];
        for ($i = 0; $i < count($columns); $i++) $str[] = " {$columns[$i]} = {$values[$i]} ";
        return implode(', ', $str);
    }

    /**
     * @param $type
     * @param $value
     * @return string
     */
    public static function smartQuoteSqlValue($type, $value)
    {
        switch ($type) {
            case self::SQL_TEXT:
                return self::qs($value) . '::' . self::SQL_TEXT;
            case self::SQL_NUMERIC:
                return self::qs($value) . '::' . self::SQL_NUMERIC;
            case self::SQL_INT:
                return self::qs($value) . '::' . self::SQL_INT;
            case self::SQL_SINT:
                return self::qs($value) . '::' . self::SQL_SINT;
            case self::SQL_JSON:
                return self::qs($value) . '::' . self::SQL_JSON;
            case self::SQL_UUID:
                return self::qs($value) . '::' . self::SQL_UUID;
            case self::SQL_INT_ARRAY:
                return self::qs($value) . '::' . self::SQL_INT_ARRAY;
            case self::SQL_DATE:
                return self::qs($value) . '::' . self::SQL_DATE;
            case self::SQL_TIME:
                return self::qs($value) . '::' . self::SQL_TIME;
            case self::SQL_VARCHAR:
                return self::qs($value) . '::' . self::SQL_VARCHAR;
            case self::SQL_VARCHAR_ARRAY:
                return self::qs($value) . '::' . self::SQL_VARCHAR_ARRAY;
            default:
                return self::qs($value);
        }
    }

    /**
     * @param $cmd
     * @return resource
     */
    public static function query($cmd, &$timer = null)
    {
        $timer = microtime(true);

        self::$last_query = $cmd;
        self::connect();

        $result = pg_query(self::$conn, $cmd);

        // Manage DEADLOCK errors
        $antibug = 0;
        while (stripos(pg_last_error(self::$conn), "deadlock detected") !== false)
        {
            $had_error = true;

            self::logError('Deadlock error on query: ' . $cmd );

            // We don't want an infinite loop!
            // So let's stop after 20 retries...
            $antibug++;
            if ( $antibug > 20 )
            {
                self::logError('Fatal error, tried too many times, failing... Query was: ' . $cmd );
                return false; // @TODO : exception ?
            }

            // We had a deadlock error, meaning that another process has to finish the query.
            // So let's WAIT during the time other process might finish the query.
            // Below the time is in MICRO SECOND meaning that 1 000 000 = 1 second.
            self::$max_delay = self::$max_delay + 1; // delay 1 second more at each pass
            $rand_delay = rand(self::$min_delay*1000000, self::$max_delay*1000000);
            self::logError('Delaying for ' . round($rand_delay / 1000000, 3) . ' seconds...' );
            usleep( $rand_delay );

            // Try again to execute the query !
            $result = pg_query(self::$conn, $cmd);
        }

        $timer = microtime(true) - $timer;

        // Log errors
        if ( isset($had_error) && $had_error )
        {
            self::logError('Query: ' . $cmd );
        }

        return $result;
    }


    /**
     * @param $str
     * @return string
     */
    public static function s($str)
    {
        return pg_escape_string($str);
    }

    /**
     * @param $str
     * @return string
     */
    public static function qs($str)
    {
        return in_array($str, ['null', 'NULL']) ? 'null' : "'" . self::s($str) . "'";
    }

    /**
     * @param $str
     * @return string
     */
    public static function qsn($str, $return_empty = false)
    {
        return in_array($str, [null, 'null', 'NULL']) ? 'null' : "'" . self::s($str) . "'";
    }

    /**
     * @param array $array
     * @param bool $store_empty_values
     * @return array
     */
    public static function toPgArray($array, $store_empty_values = false)
    {
        if(is_array($array) AND count($array)) 
        {
            $pg_array_values = '';
            foreach ($array as $value) 
            {
                if(!$value AND !$store_empty_values)
                    continue;
                if(!$value)
                    $value = '';

                $value = pg_escape_string($value);
                $pg_array_values = $pg_array_values."'$value', ";
            }
            $pg_array_values = trim($pg_array_values, ", ");
            $pg_array = "ARRAY[$pg_array_values]";
        }
        else
        {
            $pg_array = null;
        }
        return $pg_array;
    }

    /**
     * @param $cmd
     * @param $timer = null
     * @return array
     */
    public static function fetchAll($cmd, &$timer = null)
    {
        $results = [];
        $query = self::query($cmd, $timer);
        while ($row = pg_fetch_assoc($query))
        {
            $results[] = $row;
        }
        return $results;
    }

    /**
     * @param $cmd
     * @return array
     */
    public static function fetchOne($cmd, &$timer = null)
    {
        while ($row = pg_fetch_assoc(self::query($cmd, $timer))) return $row;
    }

    /**
     * @param array $rows
     * @param string $unique_col
     * @param bool $keep_unique_col
     * @return array
     */
    public static function hydrate_rootIndexes($rows, $unique_col, $keep_unique_col = true)
    {
        $newArray = array();
        foreach ($rows as &$row)
        {
            $newArray[$row[$unique_col]] = $row;
            if(!$keep_unique_col)
                unset($newArray[$row[$unique_col]][$unique_col]);
            unset($row);
        }
        return $newArray;
    }



    //////////////////////////////////////////////////
    // To be refactored in CamelCase 
    //////////////////////////////////////////////////

    /**
     * @param $cmd
     * @return bool
     */
    public static function send_query($cmd)
    {
        self::$last_query = $cmd;
        self::connect();
        $query = pg_send_query(self::$conn, $cmd);
        self::$query_result = pg_get_result(DB::$conn);
        return $query;
    }

    /**
     * @return resource
     */
    public static function get_query_result()
    {
        return self::$query_result ? self::$query_result : pg_get_result(DB::$conn);
    }

    /**
     * @return string
     */
    public static function get_query_result_state()
    {
        return pg_result_error_field(self::get_query_result(), PGSQL_DIAG_SQLSTATE);
    }

    /**
     * @return string
     */
    public static function get_query_result_error()
    {
        return pg_result_error(self::get_query_result());
    }

    /**
     * @return array
     */
    public static function fetch_assoc()
    {
        return pg_fetch_assoc(self::get_query_result());
    }

    /**
     * @param $col
     * @return string|null
     */
    public static function fetch_by_col($col)
    {
        $data = self::fetch_assoc();
        return $data && $col && isset($data[$col]) ? $data[$col] : null;
    } 




    //////////////////////////////////////////////////
    // Deprecated (not camel case)
    //////////////////////////////////////////////////

    public static function copy_from ($table_name, $column_names, $array, $options)
    {
        return copyFrom ($table_name, $column_names, $array, $options);
    }

}
